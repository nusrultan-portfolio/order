package svc

import (
	"database/sql"

	model "ecom_kg/order/model/sqlc"
	"ecom_kg/order/rpc/internal/config"
	"log"

	_ "github.com/lib/pq"
)

type ServiceContext struct {
	Config config.Config
	Store  model.Store
}

func NewServiceContext(c config.Config) *ServiceContext {

	conn, err := sql.Open("postgres", "postgresql://root:kaak@localhost:5432/ecomkg?sslmode=disable")
	if err != nil {
		log.Fatal("cannot to connect Order db:", err)
	}

	return &ServiceContext{
		Config: c,
		Store:  model.NewStore(conn),
	}
}
