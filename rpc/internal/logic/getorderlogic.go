package logic

import (
	"context"
	"database/sql"

	"ecom_kg/order/rpc/internal/svc"
	"ecom_kg/order/rpc/order"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type GetOrderLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetOrderLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetOrderLogic {
	return &GetOrderLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetOrderLogic) GetOrder(in *order.GetOrderReq) (*order.GetOrderResp, error) {
	// todo: add your logic here and delete this line

	get, err := l.svcCtx.Store.GetOrder(l.ctx, in.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, status.Errorf(codes.NotFound, "id (%v) not found", in.Id)
		}
		return nil, status.Errorf(codes.DeadlineExceeded, "%v", err)
	}

	return &order.GetOrderResp{
		Id:                get.ID,
		OrderId:           get.OrderID,
		UserId:            get.UserID,
		PaymentMethodId:   get.PaymentMethodID,
		Total:             get.Total,
		Subtotal:          get.Subtotal,
		Discount:          get.Discount,
		ShippingFee:       get.ShippingFee,
		Vat:               get.Vat,
		Trn:               get.Trn,
		OrderStatusId:     get.OrderStatusID,
		CancelReason:      get.CancelReason,
		CancelReasonNote:  get.CancelReasonNote,
		CancelledBy:       get.CancelledBy,
		CancelledAt:       get.CancelledAt,
		CancelRequestedAt: get.CancelRequestAt,
		CancelDeclinedAt:  get.CancelDeclinedAt,
		ReturnedAt:        get.ReturnedAt,
		ReturnRequestedAt: get.ReturnRequestedAt,
		ReturnDeclinedAt:  get.ReturnDeclinedAt,
		OrderConfirmedAt:  get.OrderConfirmedAt,
		ShippedAt:         get.ShippedAt,
		DeliveredAt:       get.DeliveredAt,
		Active:            get.Active,
		CreatedAt:         get.CreatedAt.String(),
		UpdatedAt:         get.UpdatedAt.String(),
	}, nil
}
