package logic

import (
	"context"
	"database/sql"
	"fmt"

	"ecom_kg/order/rpc/internal/svc"
	"ecom_kg/order/rpc/order"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type DeleteOrderLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewDeleteOrderLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DeleteOrderLogic {
	return &DeleteOrderLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *DeleteOrderLogic) DeleteOrder(in *order.DeleteOrderReq) (*order.DeleteOrderResp, error) {
	// todo: add your logic here and delete this line

	id, err := l.svcCtx.Store.DeleteOrder(l.ctx, in.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, status.Errorf(codes.NotFound, "id (%v) does not exist", in.Id)
		}
		return nil, status.Errorf(codes.DeadlineExceeded, "%v", err)
	}

	return &order.DeleteOrderResp{
		Message: fmt.Sprintf("id (%v) deleted successfully", id),
	}, nil
}
