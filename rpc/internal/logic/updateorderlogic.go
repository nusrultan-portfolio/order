package logic

import (
	"context"
	"database/sql"
	"time"

	model "ecom_kg/order/model/sqlc"
	"ecom_kg/order/rpc/internal/svc"
	"ecom_kg/order/rpc/order"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UpdateOrderLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUpdateOrderLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpdateOrderLogic {
	return &UpdateOrderLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *UpdateOrderLogic) UpdateOrder(in *order.UpdateOrderReq) (*order.UpdateOrderResp, error) {
	// todo: add your logic here and delete this line

	arg := model.UpdateOrderParams{
		ID:                in.Id,
		Total:             in.Total,
		Subtotal:          in.Subtotal,
		Discount:          in.Discount,
		ShippingFee:       in.ShippingFee,
		Vat:               in.Vat,
		Trn:               in.Trn,
		OrderStatusID:     in.OrderStatusId,
		CancelReason:      in.CancelReason,
		CancelReasonNote:  in.CancelReasonNote,
		CancelledBy:       in.CancelledBy,
		CancelledAt:       in.CancelledAt,
		CancelRequestAt:   in.CancelDeclinedAt,
		CancelDeclinedAt:  in.CancelDeclinedAt,
		ReturnedAt:        in.ReturnedAt,
		ReturnRequestedAt: in.ReturnRequestedAt,
		ReturnDeclinedAt:  in.ReturnDeclinedAt,
		OrderConfirmedAt:  in.OrderConfirmedAt,
		ShippedAt:         in.ShippedAt,
		DeliveredAt:       in.DeliveredAt,
		Active:            in.Active,
		UpdatedAt:         time.Now(),
	}

	updated, err := l.svcCtx.Store.UpdateOrder(l.ctx, arg)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, status.Errorf(codes.NotFound, "id (%v) does not exist", in.Id)
		}	
		return nil, status.Errorf(codes.DeadlineExceeded, "%v", err)
	}

	return &order.UpdateOrderResp{
		Id:                updated.ID,
		OrderId:           updated.OrderID,
		UserId:            updated.UserID,
		PaymentMethodId:   updated.PaymentMethodID,
		Total:             updated.Total,
		Subtotal:          updated.Subtotal,
		Discount:          updated.Discount,
		ShippingFee:       updated.ShippingFee,
		Vat:               updated.Vat,
		Trn:               updated.Trn,
		OrderStatusId:     updated.OrderStatusID,
		CancelReason:      updated.CancelReason,
		CancelReasonNote:  updated.CancelReasonNote,
		CancelledBy:       updated.CancelledBy,
		CancelledAt:       updated.CancelledAt,
		CancelRequestedAt: updated.CancelRequestAt,
		CancelDeclinedAt:  updated.CancelDeclinedAt,
		ReturnedAt:        updated.ReturnedAt,
		ReturnRequestedAt: updated.ReturnRequestedAt,
		ReturnDeclinedAt:  updated.ReturnDeclinedAt,
		OrderConfirmedAt:  updated.OrderConfirmedAt,
		ShippedAt:         updated.ShippedAt,
		DeliveredAt:       updated.DeliveredAt,
		Active:            updated.Active,
		CreatedAt:         updated.CreatedAt.String(),
		UpdatedAt:         updated.UpdatedAt.String(),
	}, nil
}
