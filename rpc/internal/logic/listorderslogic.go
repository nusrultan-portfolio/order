package logic

import (
	"context"

	model "ecom_kg/order/model/sqlc"
	"ecom_kg/order/rpc/internal/svc"
	"ecom_kg/order/rpc/order"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ListOrdersLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewListOrdersLogic(ctx context.Context, svcCtx *svc.ServiceContext) *ListOrdersLogic {
	return &ListOrdersLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *ListOrdersLogic) ListOrders(in *order.ListOrdersReq) (*order.ListProductsResp, error) {
	// todo: add your logic here and delete this line
	arg := model.ListOrdersParams{
		Limit:  in.PageSize,
		Offset: (in.PageId - 1) * in.PageSize,
	}

	orders, err := l.svcCtx.Store.ListOrders(l.ctx, arg)
	if err != nil {
		return nil, status.Errorf(codes.DeadlineExceeded, "%v", err)
	}

	var listOrders = make([]order.Order, len(orders))

	for i := 0; i < len(orders); i++ {
		listOrders[i].Id = orders[i].ID
		listOrders[i].OrderId = orders[i].OrderID
		listOrders[i].UserId = orders[i].UserID
		listOrders[i].PaymentMethodId = orders[i].PaymentMethodID
		listOrders[i].Total = orders[i].Total
		listOrders[i].Subtotal = orders[i].Subtotal
		listOrders[i].Discount = orders[i].Discount
		listOrders[i].ShippingFee = orders[i].ShippingFee
		listOrders[i].Vat = orders[i].Vat
		listOrders[i].Trn = orders[i].Trn
		listOrders[i].OrderStatusId = orders[i].OrderStatusID
		listOrders[i].CancelReason = orders[i].CancelReason
		listOrders[i].CancelReasonNote = orders[i].CancelReasonNote
		listOrders[i].CancelledBy = orders[i].CancelledBy
		listOrders[i].CancelledAt = orders[i].CancelledAt
		listOrders[i].CancelRequestedAt = orders[i].CancelRequestAt
		listOrders[i].CancelDeclinedAt = orders[i].CancelDeclinedAt
		listOrders[i].ReturnedAt = orders[i].ReturnedAt
		listOrders[i].ReturnRequestedAt = orders[i].ReturnRequestedAt
		listOrders[i].ReturnDeclinedAt = orders[i].ReturnDeclinedAt
		listOrders[i].OrderConfirmedAt = orders[i].OrderConfirmedAt
		listOrders[i].ShippedAt = orders[i].ShippedAt
		listOrders[i].DeliveredAt = orders[i].DeliveredAt
		listOrders[i].Active = orders[i].Active
		listOrders[i].CreatedAt = orders[i].CreatedAt.String()
		listOrders[i].UpdatedAt = orders[i].UpdatedAt.String()
	}

	respOrder := make([]*order.Order, 0)
	for i := 0; i < len(listOrders); i++ {
		respOrder = append(respOrder, &listOrders[i])
	}

	return &order.ListProductsResp{
		Orders: respOrder,
	}, nil
}
