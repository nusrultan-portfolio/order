package logic

import (
	"context"

	model "ecom_kg/order/model/sqlc"
	"ecom_kg/order/rpc/internal/svc"
	"ecom_kg/order/rpc/order"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type CreateOrderLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewCreateOrderLogic(ctx context.Context, svcCtx *svc.ServiceContext) *CreateOrderLogic {
	return &CreateOrderLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *CreateOrderLogic) CreateOrder(in *order.CreateOrderRequest) (*order.CreateOrderResponse, error) {
	// todo: add your logic here and delete this line

	arg := model.CreateOrderParams{
		OrderID:           in.OrderId,
		UserID:            in.UserId,
		PaymentMethodID:   in.PaymentMethodId,
		Total:             in.Total,
		Subtotal:          in.Subtotal,
		Discount:          in.Discount,
		ShippingFee:       in.ShippingFee,
		Vat:               in.Vat,
		Trn:               in.Trn,
		OrderStatusID:     in.OrderStatusId,
		CancelReason:      in.CancelReason,
		CancelReasonNote:  in.CancelReasonNote,
		CancelledBy:       in.CancelledBy,
		CancelledAt:       in.CancelledAt,
		CancelRequestAt:   in.CancelRequestedAt,
		CancelDeclinedAt:  in.CancelDeclinedAt,
		ReturnedAt:        in.ReturnedAt,
		ReturnRequestedAt: in.ReturnRequestedAt,
		ReturnDeclinedAt:  in.ReturnDeclinedAt,
		OrderConfirmedAt:  in.OrderConfirmedAt,
		ShippedAt:         in.ShippedAt,
		DeliveredAt:       in.DeliveredAt,
		Active:            in.Active,
	}

	created, err := l.svcCtx.Store.CreateOrder(l.ctx, arg)
	if err != nil {
		return nil, status.Errorf(codes.DeadlineExceeded, "%v", err)
	}

	return &order.CreateOrderResponse{
		Id: created.ID,
	}, nil
}
