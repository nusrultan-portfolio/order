package model

import (
	"context"
)

type Querier interface {
	CreateOrder(ctx context.Context, arg CreateOrderParams) (Order, error)
	GetOrder(ctx context.Context, id int64) (Order, error)
	ListOrders(ctx context.Context, arg ListOrdersParams) ([]Order, error)
	UpdateOrder(ctx context.Context, arg UpdateOrderParams) (Order, error)
	DeleteOrder(ctx context.Context, id int64) (int64, error)
}

var _ Querier = (*Queries)(nil)
