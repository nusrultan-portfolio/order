-- name: CreateOrder :one
INSERT INTO "orders" (
  order_id,
  user_id,
  payment_method_id,
  total,
  subtotal,
  discount,
  shipping_fee,
  vat,
  trn,
  order_status_id,
  cancel_reason,
  cancel_reason_note,
  cancelled_by,
  cancelled_at,
  cancel_request_at,
  cancel_declined_at,
  returned_at,
  return_requested_at,
  return_declined_at,
  order_confirmed_at,
  shipped_at,
  delivered_at,
  active
) VALUES (
    $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23
) RETURNING *;

-- name: GetOrder :one
SELECT * FROM orders
WHERE id = $1 LIMIT 1;

-- name: ListOrders :many
SELECT * FROM orders
ORDER BY id
LIMIT $1
OFFSET $2;


-- name: UpdateOrder :one
UPDATE orders
SET total = $2,
  subtotal = $3,
  discount = $4,
  shipping_fee = $5,
  vat = $6,
  trn = $7,
  order_status_id = $8,
  cancel_reason = $9,
  cancel_reason_note = $10,
  cancelled_by = $11,
  cancelled_at = $12,
  cancel_request_at = $13,
  cancel_declined_at = $14,
  returned_at = $15,
  return_requested_at = $16,
  return_declined_at = $17,
  order_confirmed_at = $18,
  shipped_at = $19,
  delivered_at = $20,
  active = $21,
  updated_at = $22
WHERE id = $1
RETURNING *;

-- name: DeleteOrder :one
DELETE FROM orders
WHERE id = $1
RETURNING id;


-- CREATE TABLE IF NOT EXISTS "order"(
--   "id" SERIAL PRIMARY KEY,
--   "order_id" bigint NOT NULL,
--   "user_id" bigint NOT NULL,
--   "payment_method_id" bigint NOT NULL,
--   "total" float NOT NULL,
--   "subtotal" float NOT NULL,
--   "discount" float NOT NULL,
--   "shipping_fee" float NOT NULL,
--   "vat" float NOT NULL,
--   "trn" varchar NOT NULL,
--   "order_status_id" bigint NOT NULL,
--   "cancel_reason" varchar NOT NULL,
--   "cancel_reason_note" varchar NOT NULL,
--   "cancelled_by" varchar NOT NULL,
--   "cancelled_at" varchar NOT NULL,
--   "cancel_request_at" varchar NOT NULL,
--   "cancel_declined_at" varchar NOT NULL,
--   "returned_at" varchar NOT NULL,
--   "return_requested_at" varchar NOT NULL,
--   "return_declined_at" varchar NOT NULL,
--   "order_confirmed_at" varchar NOT NULL,
--   "shipped_at" varchar NOT NULL,
--   "delivered_at" varchar NOT NULL,
--   "active" int NOT NULL,
--   "created_at" TIMESTAMPTZ NOT NULL DEFAULT (now()),
--   "updated_at" TIMESTAMPTZ NOT NULL DEFAULT (now())
-- );

-- ALTER TABLE "order" ADD FOREIGN KEY ("user_id") REFERENCES "user" ("id");